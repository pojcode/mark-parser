package com.pojcode.mark.parser.block;

public interface BlockParserTracker {
    void blockParserAdded(BlockParser blockParser);
    void blockParserRemoved(BlockParser blockParser);
}
