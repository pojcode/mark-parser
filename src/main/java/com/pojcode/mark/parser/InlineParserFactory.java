package com.pojcode.mark.parser;

import com.pojcode.mark.parser.delimiter.DelimiterProcessor;
import com.pojcode.mark.parser.internal.LinkRefProcessorData;
import com.vladsch.flexmark.util.data.DataHolder;
import org.jetbrains.annotations.NotNull;

import java.util.BitSet;
import java.util.List;
import java.util.Map;

public interface InlineParserFactory {
    InlineParser inlineParser(
            @NotNull DataHolder options,
            @NotNull BitSet specialCharacters,
            @NotNull BitSet delimiterCharacters,
            @NotNull Map<Character,
                    DelimiterProcessor> delimiterProcessors,
            @NotNull LinkRefProcessorData linkRefProcessors,
            @NotNull List<InlineParserExtensionFactory> inlineParserExtensions
    );
}
