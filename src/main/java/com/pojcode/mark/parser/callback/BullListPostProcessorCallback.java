package com.pojcode.mark.parser.callback;


import com.pojcode.mark.ast.node.TreeLinkNode;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.data.DataHolder;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@FunctionalInterface
public interface BullListPostProcessorCallback {

    /**
     * callback processor document bullList
     * @param options options
     * @param document document
     * @param nodeList nodeList
     */
    void callback(@NotNull DataHolder options, @NotNull Document document, List<TreeLinkNode> nodeList);

    /**
     * bullList post processor callback
     * @return none callback
     */
    static BullListPostProcessorCallback none() {
        return (options, document, nodeList) -> {};
    }

}
