package com.pojcode.mark.parser.processor;

import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.data.DataHolder;
import org.jetbrains.annotations.NotNull;

public abstract class NodePostProcessor implements PostProcessor {

    /**
     * @param document the node to post-process
     * @return the result of post-processing, may be a modified {@code document} argument
     */
    @NotNull
    @Override
    public Document processDocument(@NotNull Document document, @NotNull DataHolder options) {
        return document;
    }
}
