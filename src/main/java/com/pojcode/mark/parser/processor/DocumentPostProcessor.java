package com.pojcode.mark.parser.processor;

import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.ast.NodeTracker;
import com.vladsch.flexmark.util.data.DataHolder;
import org.jetbrains.annotations.NotNull;

public abstract class DocumentPostProcessor implements PostProcessor {

    /**
     * @param state node tracker used for optimizing node processing
     * @param node  the node to post-process
     */
    @Override
    public void process(@NotNull NodeTracker state, @NotNull Node node, @NotNull DataHolder options) {

    }
}
