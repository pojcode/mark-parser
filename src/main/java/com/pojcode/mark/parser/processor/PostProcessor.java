package com.pojcode.mark.parser.processor;

import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.ast.NodeTracker;
import com.vladsch.flexmark.util.data.DataHolder;
import org.jetbrains.annotations.NotNull;

public interface PostProcessor {
    /**
     * @param document the node to post-process
     * @param options dataHolder options
     * @return the result of post-processing, may be a modified {@code document} argument
     */
    @NotNull Document processDocument(@NotNull Document document, @NotNull DataHolder options);

    /**
     * @param state node tracker used for optimizing node processing
     * @param node  the node to post-process
     * @param options dataHolder options
     */
    void process(@NotNull NodeTracker state, @NotNull Node node, @NotNull DataHolder options);
}
