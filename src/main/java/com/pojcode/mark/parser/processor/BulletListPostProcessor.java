package com.pojcode.mark.parser.processor;

import com.pojcode.mark.ast.Link;
import com.pojcode.mark.ast.Text;
import com.pojcode.mark.ast.node.TreeLinkNode;
import com.pojcode.mark.parser.Parser;
import com.pojcode.mark.parser.callback.BullListPostProcessorCallback;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.collection.iteration.ReversiblePeekingIterator;
import com.vladsch.flexmark.util.data.DataHolder;
import com.pojcode.mark.utils.snowflake.SnowflakeUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * parser bulletList to Array List
*/
public class BulletListPostProcessor extends DocumentPostProcessorFactory {

    private Boolean enable;
    private Class<Node>[] blockClass;
    private Class<Node>[] itemClass;
    private Class<Node>[] childClass;
    private String rootParent;
    private BullListPostProcessorCallback processorCallback;

    public BulletListPostProcessor(DataHolder options) {
        this.enable = Parser.LISTS_ITEM_BUILD_TREE_ENABLE.get(options);
        this.blockClass = Parser.LISTS_ITEM_BUILD_TREE_BLOCK_CLASS.get(options);
        this.itemClass = Parser.LISTS_ITEM_BUILD_TREE_ITEM_CLASS.get(options);
        this.childClass = Parser.LISTS_ITEM_BUILD_TREE_CHILD_CLASS.get(options);
        this.rootParent = Parser.LISTS_ITEM_BUILD_TREE_ROOT_PARENT.get(options);
        if (options != null) {
            this.processorCallback = Parser.LISTS_ITEM_BUILD_TREE_PROCESSOR_CALLBACK.get(options);
        }
    }


    @Override
    public @NotNull PostProcessor apply(@NotNull Document document) {
        return new DocumentPostProcessor() {
            @Override
            public @NotNull Document processDocument(@NotNull Document document, @NotNull DataHolder options) {
                if (Boolean.TRUE.equals(getEnable())) {
                    List<TreeLinkNode> nodeList = new ArrayList<>();
                    recursionNodes(document, node -> {
                        if (isAssignableFromClasses(getBlockClass(), node.getClass())) {
                            handlerBulletList(node, getRootParent(), nodeList);
                        }
                    });
                    getProcessorCallback().callback(options, document, nodeList);
                }
                return document;
            }
        };
    }



    /**
     * recursion node for ReversiblePeekingIterable
     * @param node 节点
     * @param consumer consumer node
     */
    protected void recursionNodes(Node node, Consumer<Node> consumer) {
        ReversiblePeekingIterator<Node> iterator = node.getChildIterator();
        while (iterator.hasNext()) {
            Node curNode = iterator.next();
            consumer.accept(curNode);
        }
    }

    /**
     * recursion bulletList
     * @param node 节点
     * @param parentId 父节点ID
     * @param treeLinkNodes 存储节点列表
     * @return parentId
     */
    protected String handlerBulletList(Node node, String parentId, List<TreeLinkNode> treeLinkNodes) {
        ReversiblePeekingIterator<Node> iterator = node.getChildIterator();
        String prevPid = parentId;
        while (iterator.hasNext()) {
            Node next = iterator.next();
            if (isAssignableFromClasses(getBlockClass(), next.getClass())) {
                prevPid = handlerBulletList(next, prevPid, treeLinkNodes);
            } else if (isAssignableFromClasses(getItemClass(), next.getClass())) {
                prevPid = handlerBulletList(next, parentId, treeLinkNodes);
            } else if (isAssignableFromClasses(getChildClass(), next.getClass())) {
                if (next instanceof Link && Objects.nonNull(next.getFirstChild())) {
                    TreeLinkNode treeLinkNode = new TreeLinkNode((Link) next);
                    treeLinkNode.setId(SnowflakeUtil.nextIdStr());
                    treeLinkNode.setParentId(parentId);
                    treeLinkNodes.add(treeLinkNode);
                    return treeLinkNode.getId();
                } else if (next instanceof Text) {
                    TreeLinkNode treeLinkNode = new TreeLinkNode((Text) next);
                    treeLinkNode.setId(SnowflakeUtil.nextIdStr());
                    treeLinkNode.setParentId(parentId);
                    treeLinkNodes.add(treeLinkNode);
                    return treeLinkNode.getId();
                }
            }
        }
        return parentId;
    }

    /**
     * class isAssignableFrom
     * @param classes class
     * @param targetClass target class
     * @return true or false
     */
    protected boolean isAssignableFromClasses(Class<Node>[] classes, Class targetClass) {
        if (classes == null || classes.length == 0) {
            return false;
        }
        for (Class<Node> aClass : classes) {
            if (aClass.isAssignableFrom(targetClass)) {
                return true;
            }
        }
        return false;
    }

    public Boolean getEnable() {
        return enable;
    }

    public Class<Node>[] getBlockClass() {
        return blockClass;
    }

    public Class<Node>[] getItemClass() {
        return itemClass;
    }

    public Class<Node>[] getChildClass() {
        return childClass;
    }

    public String getRootParent() {
        return rootParent;
    }

    public BullListPostProcessorCallback getProcessorCallback() {
        return processorCallback;
    }
}
