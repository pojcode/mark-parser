package com.pojcode.mark.parser.processor;

import com.pojcode.mark.parser.Parser;
import com.vladsch.flexmark.util.data.DataHolder;
import com.vladsch.flexmark.util.data.DataKey;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class DefaultPostProcessorFactory {

    private static List<PostProcessorFactory> postProcessorFactories = new ArrayList<>();
    private static Map<DataKey<PostProcessorFactory>, Function<DataHolder, PostProcessorFactory>> postProcessorFactoryMap = new HashMap<>();

    static {
        postProcessorFactoryMap.put(Parser.LISTS_ITEM_BUILD_TREE_PROCESSOR_FACTORY, BulletListPostProcessor::new);
    }

    public static void initCalculatePostProcessors(@NotNull DataHolder options) {
        postProcessorFactories.clear();
        for (DataKey<PostProcessorFactory> dataKey : postProcessorFactoryMap.keySet()) {
            PostProcessorFactory postProcessorFactory = postProcessorFactoryMap.get(dataKey)
                    .apply(options);
            postProcessorFactories.add(postProcessorFactory);
        }
    }

    public static List<PostProcessorFactory> getPostProcessorFactories() {
        return postProcessorFactories;
    }

}
