/**
 * Parse input text to AST nodes see {@link com.pojcode.mark.parser.Parser}
 */
package com.pojcode.mark.parser;
