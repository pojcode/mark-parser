package com.pojcode.mark.utils.snowflake;

/**
 * Snowflake Util
 */
public class SnowflakeUtil {

    /**
     * Twitter_Snowflake 算法器
     */
    private static SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);

    /**
     * 唯一ID生成
     * @return long
     */
    public static long nextIdLong() {
        return idWorker.nextId();
    }

    public static String nextIdStr() {
        return Long.toString(idWorker.nextId());
    }

}
