package com.pojcode.mark.utils.tree;

import com.pojcode.mark.utils.tree.parser.CommonNodeParser;
import com.pojcode.mark.utils.tree.parser.NodeParser;

import java.util.List;

/**
 * Tree Util
*/
public class TreeUtil {

    private TreeUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    /**
     * 构建列表树
     * @param list 源数据
     * @param <T> 节点泛型
     * @return 列表树
     */
    public static <T extends Node> List<TreeNode> build(List<T> list) {
        return build(list, "");
    }

    /**
     * 构建列表树
     * @param list 源数据
     * @param rootId root节点parentId标识
     * @param <T> 节点泛型
     * @return 列表树
     */
    public static <T extends Node> List<TreeNode> build(List<T> list, String rootId) {
        return build(list, rootId, new CommonNodeParser<>());
    }

    /**
     * 构建列表树
     * @param list 源数据
     * @param rootId root节点parentId标识
     * @param nodeParser 节点解析器
     * @param <T> 节点泛型
     * @return 列表树
     */
    public static <T extends Node> List<TreeNode> build(List<T> list, String rootId, NodeParser<T> nodeParser) {
        return build(list, rootId, TreeConfig.DEFAULT_CONFIG, nodeParser);
    }

    /**
     * 构建列表树
     * @param list 源数据
     * @param config 树构建配置
     * @param <T> 节点泛型
     * @return 列表树
     */
    public static <T extends Node> List<TreeNode> build(List<T> list, TreeConfig config) {
        return build(list, "", config);
    }

    /**
     * 构建列表树
     * @param list 源数据
     * @param rootId root节点parentId标识
     * @param config 树构建配置
     * @param <T> 节点泛型
     * @return 列表树
     */
    public static <T extends Node> List<TreeNode> build(List<T> list, String rootId, TreeConfig config) {
        return build(list, rootId, config, new CommonNodeParser<>());
    }

    /**
     * 构建列表树
     * @param list 源数据
     * @param rootId root节点parentId标识
     * @param config 树构建配置
     * @param nodeParser 节点解析器
     * @param <T> 节点泛型
     * @return 列表树
     */
    public static <T extends Node> List<TreeNode> build(List<T> list, String rootId, TreeConfig config, NodeParser<T> nodeParser) {
        return TreeBuilder.of(rootId, config)
                .append(list, nodeParser)
                .build();
    }

}
