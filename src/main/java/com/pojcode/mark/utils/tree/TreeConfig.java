package com.pojcode.mark.utils.tree;

import com.pojcode.mark.utils.tree.merge.MergePolicy;
import com.pojcode.mark.utils.tree.merge.NodeMergePolicy;

import java.io.Serializable;

/**
 * Tree Config
 */
public class TreeConfig implements Serializable {

	/**
	 * 默认属性配置对象
	 */
	public static TreeConfig DEFAULT_CONFIG = new TreeConfig();

	private String idKey = "id";
	private String parentIdKey = "parentId";
	private String weightKey = "weight";
	private String nameKey = "name";
	private String childrenKey = "children";
	/**
	 * 最大树深度，根节点为0，从0开始，如果超出将采取合并策略
	 */
	private Integer maxDeep;
	private MergePolicy mergePolicy = NodeMergePolicy.DISCARD;

	/**
	 * 获取ID对应的名称
	 *
	 * @return ID对应的名称
	 */
	public String getIdKey() {
		return this.idKey;
	}

	/**
	 * 设置ID对应的名称
	 *
	 * @param idKey ID对应的名称
	 * @return this
	 */
	public TreeConfig setIdKey(String idKey) {
		this.idKey = idKey;
		return this;
	}

	/**
	 * 获取权重对应的名称
	 *
	 * @return 权重对应的名称
	 */
	public String getWeightKey() {
		return this.weightKey;
	}

	/**
	 * 设置权重对应的名称
	 *
	 * @param weightKey 权重对应的名称
	 * @return this
	 */
	public TreeConfig setWeightKey(String weightKey) {
		this.weightKey = weightKey;
		return this;
	}

	/**
	 * 获取节点名对应的名称
	 *
	 * @return 节点名对应的名称
	 */
	public String getNameKey() {
		return this.nameKey;
	}

	/**
	 * 设置节点名对应的名称
	 *
	 * @param nameKey 节点名对应的名称
	 * @return this
	 */
	public TreeConfig setNameKey(String nameKey) {
		this.nameKey = nameKey;
		return this;
	}

	/**
	 * 获取子点对应的名称
	 *
	 * @return 子点对应的名称
	 */
	public String getChildrenKey() {
		return this.childrenKey;
	}

	/**
	 * 设置子点对应的名称
	 *
	 * @param childrenKey 子点对应的名称
	 * @return this
	 */
	public TreeConfig setChildrenKey(String childrenKey) {
		this.childrenKey = childrenKey;
		return this;
	}

	/**
	 * 获取父节点ID对应的名称
	 *
	 * @return 父点对应的名称
	 */
	public String getParentIdKey() {
		return this.parentIdKey;
	}


	/**
	 * 设置父点对应的名称
	 *
	 * @param parentIdKey 父点对应的名称
	 * @return this
	 */
	public TreeConfig setParentIdKey(String parentIdKey) {
		this.parentIdKey = parentIdKey;
		return this;
	}

	/**
	 * 获取最大支持深度
	 * @return 深度
	 */
	public Integer getMaxDeep() {
		return maxDeep;
	}

	/**
	 * 设置树最大深度
	 * @param maxDeep 最大深度
	 * @return this
	 */
	public TreeConfig setMaxDeep(Integer maxDeep) {
		this.maxDeep = maxDeep;
		return this;
	}

	/**
	 * 获取树节点合并策略
	 * @return 合并策略
	 */
	public MergePolicy getMergePolicy() {
		return mergePolicy;
	}

	/**
	 * 设置节点合并策略
	 * @param mergePolicy 合并策略
	 * @return this
	 */
	public TreeConfig setMergePolicy(MergePolicy mergePolicy) {
		this.mergePolicy = mergePolicy;
		return this;
	}
}
