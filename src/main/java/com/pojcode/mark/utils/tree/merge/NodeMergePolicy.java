package com.pojcode.mark.utils.tree.merge;


import java.util.function.Consumer;

/**
 * Node Merge Policy
 */
public enum NodeMergePolicy implements MergePolicy {
	/**
	 * 向上合并
	 */
	UP(TreeNodeUpMergePolicy.INSTANCE::merge),
	/**
	 * 丢弃
	 */
	DISCARD(TreeNodeDiscardMergePolicy.INSTANCE::merge),
	;

	NodeMergePolicy(Consumer<MergePolicyContext> consumer) {
		this.consumer = consumer;
	}

	/**
	 * consumer tree list
	 */
	private Consumer<MergePolicyContext> consumer;

	@Override
	public void merge(MergePolicyContext mergePolicyContext) {
		consumer.accept(mergePolicyContext);
	}
}