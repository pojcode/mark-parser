package com.pojcode.mark.utils.tree;


import com.pojcode.mark.utils.tree.parser.NodeExtraField;

import java.io.Serializable;

/**
 * Node
*/
public interface Node extends NodeExtraField, Comparable<Node>, Serializable {

	/**
	 * 获取ID
	 * @return ID
	 */
	String getId();

	/**
	 * 设置ID
	 *
	 * @param id ID
	 * @return this
	 */
	Node setId(String id);

	/**
	 * 获取父节点ID
	 * @return 父节点ID
	 */
	String getParentId();

	/**
	 * 设置父节点ID
	 *
	 * @param parentId 父节点ID
	 * @return this
	 */
	Node setParentId(String parentId);

	/**
	 * 获取节点名称
	 *
	 * @return 节点标签名称
	 */
	CharSequence getName();

	/**
	 * 设置节点名称
	 *
	 * @param name 节点标签名称
	 * @return this
	 */
	Node setName(CharSequence name);

	/**
	 * 获取权重
	 *
	 * @return 权重
	 */
	Comparable<?> getWeight();

	/**
	 * 设置权重
	 *
	 * @param weight 权重
	 * @return this
	 */
	Node setWeight(Comparable<?> weight);

	@Override
	default String[] extraFields() {
		return new String[0];
	}

	/**
	 * node节点比较
	 * @param node node
	 * @return int
	 */
	@SuppressWarnings({"unchecked", "rawtypes", "NullableProblems"})
	@Override
	default int compareTo(Node node) {
		final Comparable c1 = this.getWeight();
		final Comparable c2 = node.getWeight();
		if (c1 == c2) {
			return 0;
		} else if (c1 == null) {
			return -1;
		} else if (c2 == null) {
			return 1;
		}
		return c1.compareTo(c2);
	}
}
