package com.pojcode.mark.utils.tree.merge;

import com.pojcode.mark.utils.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 树节点向上合并
*/
public class TreeNodeUpMergePolicy extends AbstractMergePolicy {

    public static final MergePolicy INSTANCE = new TreeNodeUpMergePolicy();

    @Override
    protected void merge(List<TreeNode> treeList, MergePolicyConfig config) {
        Integer maxDeep = config.getMaxDeep();
        for (TreeNode treeNode : treeList) {
            if (treeNode.getMaxDeep() > maxDeep) {
                mergeOverDeepNodes(treeNode, maxDeep);
            }
        }
    }

    /**
     * 获取超出树深度节点列表
     * @param treeNode 树节点
     * @param maxDeep 最大深度
     */
    protected void mergeOverDeepNodes(TreeNode treeNode, Integer maxDeep) {
        treeNode.breadthWalk(node -> {
            if (node.getDeep() >= maxDeep) {
                List<TreeNode> overNodeList = new ArrayList<>();
                disassociateNodes(node.getChildren(), overNodeList);
                node.setChildren(null);
                if (node.getParent() != null) {
                    node.getParent().addChildren(overNodeList);
                }
            }
        });
    }

    /**
     * 取消节点关联关系组成列表
     * @param treeNodes 树节点列表
     * @param container 装置容器
     */
    private void disassociateNodes(List<TreeNode> treeNodes, List<TreeNode> container) {
        if (Objects.isNull(treeNodes) || treeNodes.isEmpty()) {
            return;
        }
        for (TreeNode treeNode : treeNodes) {
            container.add(treeNode);
            disassociateNodes(treeNode.getChildren(), container);
            treeNode.setChildren(null);
        }
    }

}
