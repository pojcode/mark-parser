package com.pojcode.mark.utils.tree;


import com.pojcode.mark.utils.StringUtil;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 树节点，根据权重排序，相同默认顺序排序
*/
public class TreeNode extends LinkedHashMap<String, Object> implements Node, Order, Serializable {
    private static final long serialVersionUID = 1L;

    private final TreeConfig treeConfig;

    private TreeNode parent;
    private Integer order;

    public TreeNode() {
        this(null);
    }

    public TreeNode(TreeConfig treeConfig) {
        this.treeConfig = Optional.ofNullable(treeConfig)
                .orElse(TreeConfig.DEFAULT_CONFIG);
    }

    public TreeConfig getTreeConfig() {
        return treeConfig;
    }

    public TreeNode getParent() {
        return this.parent;
    }

    @Override
    public Integer getOrder() {
        return Optional.ofNullable(this.order).orElse(0);
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    /**
     * 设置父节点
     * @param parent 父节点
     * @return this
     */
    public TreeNode setParent(TreeNode parent) {
        this.parent = parent;
        if (Objects.nonNull(parent)) {
            this.setParentId(parent.getId());
        }
        return this;
    }

    @Override
    public String getId() {
        return (String) this.get(treeConfig.getIdKey());
    }

    @Override
    public Node setId(String id) {
        this.put(treeConfig.getIdKey(), id);
        return this;
    }

    @Override
    public String getParentId() {
        return (String) this.get(treeConfig.getParentIdKey());
    }

    @Override
    public Node setParentId(String parentId) {
        this.put(treeConfig.getParentIdKey(), parentId);
        return this;
    }

    @Override
    public CharSequence getName() {
        return (CharSequence) this.get(treeConfig.getNameKey());
    }

    @Override
    public Node setName(CharSequence name) {
        this.put(treeConfig.getNameKey(), name);
        return this;
    }

    @Override
    public Comparable<?> getWeight() {
        return (Comparable<?>) this.get(treeConfig.getWeightKey());
    }

    @Override
    public Node setWeight(Comparable<?> weight) {
        this.put(treeConfig.getWeightKey(), weight);
        return this;
    }

    /**
     * 获取所有子节点
     *
     * @return 所有子节点
     */
    public List<TreeNode> getChildren() {
        return (List<TreeNode>) this.get(treeConfig.getChildrenKey());
    }

    /**
     * 设置子节点，设置后会覆盖所有原有子节点
     *
     * @param children 子节点列表
     * @return this
     */
    public TreeNode setChildren(List<TreeNode> children) {
        if(null == children){
            this.remove(treeConfig.getChildrenKey());
        }
        this.put(treeConfig.getChildrenKey(), children);
        return this;
    }

    /**
     * 增加子节点，同时关联子节点的父节点为当前节点
     * @param children 子节点列表
     * @return this
     */
    public final TreeNode addChildren(List<TreeNode> children) {
        if (Objects.nonNull(children) && !children.isEmpty()) {
            List<TreeNode> childrenList = this.getChildren();
            if (childrenList == null) {
                childrenList = new ArrayList<>();
                setChildren(childrenList);
            }
            int childSize = childrenList.size();
            List<TreeNode> newChildren = children.stream().sorted(Node::compareTo).collect(Collectors.toList());
            for (int i = 0; i < newChildren.size(); i++) {
                TreeNode child = newChildren.get(i);
                child.setParent(this);
                child.setOrder(childSize + i);
                childrenList.add(child);
            }
        }
        return this;
    }

    /**
     * 增加子节点，同时关联子节点的父节点为当前节点
     * @param children 子节点列表
     * @return this
     */
    public final TreeNode addChildren(TreeNode... children) {
        return addChildren(Arrays.asList(children));
    }

    /**
     * 扩展属性
     * @param key key
     * @param value value
     */
    public void putExtra(String key, Object value) {
        this.put(key, value);
    }

    /**
     * 查询当前节点所在深度，根节点0
     * @return 当前节点深度
     */
    public Integer getDeep() {
        return getLevel() - 1;
    }

    /**
     * 查询节点最大深度，根节点0
     * @return 最大深度
     */
    public Integer getMaxDeep() {
        return getMaxLevel() - 1;
    }

    /**
     * 查询节点层级，根节点为1
     * @return 当前层级
     */
    public Integer getLevel() {
        int level = 1;
        TreeNode current = this;
        while (current.getParent() != null) {
            level++;
            current = current.getParent();
        }
        return level;
    }

    /**
     * 查询节点最大层数，根节点为1
     * @return 最大层数
     */
    public Integer getMaxLevel() {
        List<TreeNode> children = this.getChildren();
        if (children == null || children.isEmpty()) {
            return 1;
        }
        int max = 0;
        for (TreeNode child : children) {
            int level = child.getMaxLevel();
            max = Math.max(max, level);
        }
        return max + 1;
    }

    /**
     * 获取parentName
     * @return parentName
     */
    public List<CharSequence> getParentName() {
        List<CharSequence> parentNames = new ArrayList<>();
        TreeNode parent = getParent();
        while (parent != null) {
            parentNames.add(parent.getName());
            parent = parent.getParent();
        }
        Collections.reverse(parentNames);
        return parentNames;
    }

    /**
     * 深度递归并处理子树下的节点
     * @param consumer 节点处理器
     */
    public void depthWalk(Consumer<TreeNode> consumer) {
        consumer.accept(this);
        final List<TreeNode> children = getChildren();
        if (children != null && !children.isEmpty()) {
            children.forEach(tree -> tree.depthWalk(consumer));
        }
    }

    /**
     * 广度遍历并处理子树下的节点
     * @param consumer 节点处理器
     */
    public void breadthWalk(Consumer<TreeNode> consumer) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(this);
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            consumer.accept(node);
            final List<TreeNode> children = node.getChildren();
            if (children != null && !children.isEmpty()) {
                children.forEach(queue::offer);
            }
        }
    }

    /**
     * 打印
     * @param treeNode 节点
     * @param writer 输出write
     * @param intent 缩进量
     */
    public void print(TreeNode treeNode, PrintWriter writer, int intent) {
        writer.println(String.format("%s%s[%s]", StringUtil.repeat(StringUtil.SPACE, intent * 2), treeNode.getId(), treeNode.getName()));
        writer.flush();
        final List<TreeNode> children = treeNode.getChildren();
        if (children != null && !children.isEmpty()) {
            children.forEach(node -> node.print(node, writer, intent + 2));
        }
    }

    @Override
    public String toString() {
        final StringWriter stringWriter = new StringWriter();
        print(this, new PrintWriter(stringWriter), 0);
        return stringWriter.toString();
    }

}
