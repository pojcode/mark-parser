package com.pojcode.mark.utils.tree.merge;

/**
 * Merge Policy Config
*/
public class MergePolicyConfig {

    /**
     * 最大深度限制，根节点
     */
    private Integer maxDeep;

    public MergePolicyConfig(Integer maxDeep) {
        this.maxDeep = maxDeep;
    }

    public static MergePolicyConfig of() {
        return new MergePolicyConfig(null);
    }

    public static MergePolicyConfig of(Integer maxDeep) {
        return new MergePolicyConfig(maxDeep);
    }

    public MergePolicyConfig maxDeep(Integer maxDeep) {
        this.maxDeep = maxDeep;
        return this;
    }

    public Integer getMaxDeep() {
        return maxDeep;
    }
}
