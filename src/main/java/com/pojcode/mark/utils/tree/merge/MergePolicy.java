package com.pojcode.mark.utils.tree.merge;


/**
 * Merge Policy
*/
@FunctionalInterface
public interface MergePolicy {

    /**
     * 合并操作
     * @param mergePolicyContext mergePolicyContext
     */
    void merge(MergePolicyContext mergePolicyContext);

}
