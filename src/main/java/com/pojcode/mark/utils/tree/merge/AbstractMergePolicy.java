package com.pojcode.mark.utils.tree.merge;

import com.pojcode.mark.utils.tree.TreeNode;

import java.util.List;

/**
 * abstract merge policy
*/
public abstract class AbstractMergePolicy implements MergePolicy {

    @Override
    public void merge(MergePolicyContext mergePolicyContext) {
        MergePolicyConfig config = mergePolicyContext.getConfig();
        if (config == null || config.getMaxDeep() == null || config.getMaxDeep() < 0) {
            return;
        }
        merge(mergePolicyContext.getTreeList(), mergePolicyContext.getConfig());
    }

    /**
     * merge
     * @param treeList treeList
     * @param config config
     */
    protected abstract void merge(List<TreeNode> treeList, MergePolicyConfig config);

}
