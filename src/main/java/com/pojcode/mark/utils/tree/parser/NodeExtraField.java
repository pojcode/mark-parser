package com.pojcode.mark.utils.tree.parser;

/**
 * 扩展属性实现
*/
public interface NodeExtraField {

    /**
     * 扩展属性列表
     * @return extra field array
     */
    String[] extraFields();

}
