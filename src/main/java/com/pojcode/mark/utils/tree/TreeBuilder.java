package com.pojcode.mark.utils.tree;

import com.pojcode.mark.utils.Assert;
import com.pojcode.mark.utils.StringUtil;
import com.pojcode.mark.utils.tree.merge.MergePolicyConfig;
import com.pojcode.mark.utils.tree.merge.MergePolicyContext;
import com.pojcode.mark.utils.tree.merge.MergePolicy;
import com.pojcode.mark.utils.tree.parser.NodeParser;

import java.util.*;

/**
 * Tree Builder
*/
public class TreeBuilder {

    /**
     * 顶层根节点parentId
     */
    private String rootId;
    private TreeConfig treeConfig;
    private Map<String, TreeNode> nodeMap;
    private List<TreeNode> treeList;
    private boolean isBuild;

    public TreeBuilder(String rootId, TreeConfig treeConfig) {
        this.rootId = rootId;
        this.treeConfig = treeConfig;
        this.nodeMap = new LinkedHashMap<>();
        this.treeList = new LinkedList<>();
        this.isBuild = false;
    }

    public TreeBuilder setRootId(String rootId) {
        this.rootId = rootId;
        return this;
    }

    public TreeBuilder setTreeConfig(TreeConfig treeConfig) {
        this.treeConfig = treeConfig;
        return this;
    }

    /**
     * 创建树构建器
     * @param rootId rootId
     * @param treeConfig config配置
     * @return this
     */
    public static TreeBuilder of(String rootId, TreeConfig treeConfig) {
        return new TreeBuilder(rootId, treeConfig);
    }

    /**
     * append treeMap
     * @param treeMap 节点map
     * @return this
     */
    public TreeBuilder append(Map<String, TreeNode> treeMap) {
        checkBuilt();
        treeMap = Optional.ofNullable(treeMap).orElse(Collections.emptyMap());
        this.nodeMap.putAll(treeMap);
        return this;
    }

    /**
     * append nodeList
     * @param nodeList 节点列表
     * @param nodeParser 节点解析器
     * @param <T> 节点泛型
     * @return this
     */
    public <T extends Node> TreeBuilder append(List<T> nodeList, NodeParser<T> nodeParser) {
        checkBuilt();
        nodeList = Optional.ofNullable(nodeList).orElse(Collections.emptyList());
        final Map<String, TreeNode> map = new LinkedHashMap<>(nodeList.size(), 1);
        for (T node : nodeList) {
            TreeNode treeNode = new TreeNode(this.treeConfig);
            nodeParser.parse(node, treeNode);
            map.put(treeNode.getId(), treeNode);
        }
        return append(map);
    }

    /**
     * 重置Builder，清空节点列表
     * @return this
     */
    public TreeBuilder reset() {
        this.treeList.clear();
        this.isBuild = false;
        return this;
    }

    /**
     * 构建树
     * @return this
     */
    public List<TreeNode> build() {
        if (isBuild) {
            return this.treeList;
        }

        sortNodeMap(false);
        buildTree();
        postTreeCut();

        this.isBuild = true;
        return this.treeList;
    }

    /**
     * 节点排序
     * @param isDesc 是否降序
     * @return void
     */
    private void sortNodeMap(Boolean isDesc) {
        Map<String, TreeNode> result = new LinkedHashMap<>();
        Comparator<Node> entryComparator = Node::compareTo;
        if (Boolean.TRUE.equals(isDesc)) {
            entryComparator = entryComparator.reversed();
        }
        this.nodeMap.values().stream().sorted(entryComparator)
                .forEachOrdered(treeNode -> result.put(treeNode.getId(), treeNode));
        this.nodeMap = result;
    }

    /**
     * build tree
     */
    private void buildTree() {
        if (this.nodeMap.isEmpty()) {
            return;
        }
        for (TreeNode node : this.nodeMap.values()) {
            if (node == null) {
                continue;
            }
            if (StringUtil.equals(this.rootId, node.getParentId())) {
                node.setOrder(this.treeList.size());
                this.treeList.add(node);
                continue;
            }
            final TreeNode parentNode = this.nodeMap.get(node.getParentId());
            if (parentNode != null) {
                parentNode.addChildren(node);
            }
        }
    }

    /**
     * 后置处理
     */
    private void postTreeCut() {
        MergePolicy mergePolicy = this.treeConfig.getMergePolicy();
        if (Objects.nonNull(mergePolicy)) {
            MergePolicyConfig config = MergePolicyConfig.of();
            config.maxDeep(this.treeConfig.getMaxDeep());
            MergePolicyContext context = new MergePolicyContext(this.treeList, config);
            mergePolicy.merge(context);
        }
    }

    /**
     * 检查是否已构建
     */
    private void checkBuilt() {
        Assert.isTrue(!isBuild, "Current tree has been built");
    }

}
