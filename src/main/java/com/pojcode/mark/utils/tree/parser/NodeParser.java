package com.pojcode.mark.utils.tree.parser;

import com.pojcode.mark.utils.tree.Node;
import com.pojcode.mark.utils.tree.TreeNode;

/**
 * 节点解析器
*/
@FunctionalInterface
public interface NodeParser<T extends Node> {
	/**
	 * 节点转换
	 * @param targetNode   源数据实体
	 * @param treeNode 树节点实体
	 */
	void parse(T targetNode, TreeNode treeNode);
}