package com.pojcode.mark.utils.tree;

/**
 * Node Order
*/
public interface Order {

    /**
     * 获取顺序值，从0开始
     * @return 顺序值
     */
    Integer getOrder();

}
