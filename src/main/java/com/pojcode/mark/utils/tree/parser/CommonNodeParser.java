package com.pojcode.mark.utils.tree.parser;

import com.pojcode.mark.utils.tree.Node;
import com.pojcode.mark.utils.tree.TreeNode;
import com.pojcode.mark.utils.ReflectUtil;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 通用节点解析器，反射添加所有扩展属性
*/
public class CommonNodeParser<T extends Node> implements NodeParser<T> {

    @Override
    public void parse(T node, TreeNode treeNode) {
        treeNode.setId(node.getId());
        treeNode.setParentId(node.getParentId());
        treeNode.setName(node.getName());
        treeNode.setWeight(node.getWeight());

        String[] extraFields = node.extraFields();
        if (extraFields == null || extraFields.length == 0) {
            Map<String, Field> fieldMap = ReflectUtil.getFieldMap(node);
            extraFields = fieldMap.keySet().toArray(new String[0]);
        }
        for (String fieldName : extraFields) {
            Object fieldVal = ReflectUtil.getValueByFieldName(node, fieldName);
            treeNode.putExtra(fieldName, fieldVal);
        }
    }

}
