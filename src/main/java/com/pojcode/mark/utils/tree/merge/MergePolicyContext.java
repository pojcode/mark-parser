package com.pojcode.mark.utils.tree.merge;

import com.pojcode.mark.utils.tree.TreeNode;

import java.util.List;

/**
 *
*/
public class MergePolicyContext {

    private List<TreeNode> treeList;
    private MergePolicyConfig config;

    public MergePolicyContext(List<TreeNode> treeList, MergePolicyConfig config) {
        this.treeList = treeList;
        this.config = config;
    }

    public static MergePolicyContext of(List<TreeNode> treeList, MergePolicyConfig config) {
        return new MergePolicyContext(treeList, config);
    }

    public List<TreeNode> getTreeList() {
        return treeList;
    }

    public MergePolicyContext setTreeList(List<TreeNode> treeList) {
        this.treeList = treeList;
        return this;
    }

    public MergePolicyConfig getConfig() {
        return config;
    }

    public void setConfig(MergePolicyConfig config) {
        this.config = config;
    }
}
