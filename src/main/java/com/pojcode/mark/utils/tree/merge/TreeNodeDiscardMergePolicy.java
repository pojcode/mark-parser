package com.pojcode.mark.utils.tree.merge;

import com.pojcode.mark.utils.tree.TreeNode;

import java.util.List;

/**
 * 树节点舍弃合并
*/
public class TreeNodeDiscardMergePolicy extends AbstractMergePolicy {

    public static final MergePolicy INSTANCE = new TreeNodeDiscardMergePolicy();

    @Override
    protected void merge(List<TreeNode> treeList, MergePolicyConfig config) {
        Integer maxDeep = config.getMaxDeep();
        treeList.forEach(treeNode -> discardTree(treeNode, 0, maxDeep));
    }

    /**
     * 树的节点去除
     * @param treeNode 节点
     * @param currentDeep 当前深度
     * @param maxDeep 最大深度
     */
    protected void discardTree(TreeNode treeNode, Integer currentDeep, Integer maxDeep) {
        if (treeNode == null) {
            return;
        }
        if (currentDeep.equals(maxDeep)) {
            treeNode.setChildren(null);
            return;
        }
        final List<TreeNode> children = treeNode.getChildren();
        if (children != null && !children.isEmpty()) {
            children.forEach(childNode -> discardTree(childNode, currentDeep + 1, maxDeep));
        }
    }

}
