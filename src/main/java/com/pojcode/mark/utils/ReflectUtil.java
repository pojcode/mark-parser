package com.pojcode.mark.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Reflect Util
 */
public class ReflectUtil {

    private static final Logger logger = LoggerFactory.getLogger(ReflectUtil.class);

    private ReflectUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    /**
     * 获取obj对象fieldName的Field
     * @param obj       对象信息
     * @param fieldName 名称
     * @return 名称
     */
    public static Field getFieldByFieldName(Object obj, String fieldName) {
        if (obj == null || fieldName == null) {
            return null;
        }
        for (Class<?> superClass = obj.getClass(); superClass != Object.class; superClass = superClass
                .getSuperclass()) {
            try {
                return superClass.getDeclaredField(fieldName);
            } catch (Exception e) {
                logger.error("获取obj对象fieldName的Field异常", e);
            }
        }
        return null;
    }

    public static Map<String, Field> getFieldMap(Object obj) {
        if (obj == null) {
            return Collections.emptyMap();
        }
        Map<String, Field> map = new HashMap<>();
        for (Class<?> superClass = obj.getClass(); superClass != Object.class; superClass = superClass
                .getSuperclass()) {
            try {
                Field[] fs = superClass.getDeclaredFields();
                for (Field f : fs) {
                    if (!(map.containsKey(f.getName()))) {
                        map.put(f.getName(), f);
                    }
                }
            } catch (Exception e) {
                logger.error("getFieldMap异常", e);
            }
        }
        return map;
    }

    /**
     * 获取obj对象fieldName的属性值
     * @param obj       对象信息
     * @param fieldName 名称
     * @return 属性值
     */
    public static Object getValueByFieldName(Object obj, String fieldName) {
        Object value = null;
        try {
            Field field = getFieldByFieldName(obj, fieldName);
            if (field != null) {
                makeAccessible(field);
                value = field.get(obj);
            }
        } catch (Exception e) {
            logger.error("获取obj对象fieldName的属性值异常", e);
        }
        return value;
    }

    /**
     * @param obj       对象信息
     * @param fieldType 类型
     * @param <T>       泛型
     * @return 泛型
     */
    public static <T> T getValueByFieldType(Object obj, Class<T> fieldType) {
        Object value = null;
        for (Class<?> superClass = obj.getClass(); superClass != Object.class; superClass = superClass
                .getSuperclass()) {
            try {
                Field[] fields = superClass.getDeclaredFields();
                for (Field f : fields) {
                    if (f.getType() == fieldType) {
                        makeAccessible(f);
                        value = f.get(obj);
                        break;
                    }
                }
                if (value != null) {
                    break;
                }
            } catch (Exception e) {
                logger.error("getValueByFieldType error", e);
            }
        }
        return (T) value;
    }


    /**
     * 设置obj对象fieldName的属性值
     * @param obj       对象信息
     * @param fieldName 名称
     * @param value     值
     * @return 设置结果
     */
    public static boolean setValueByFieldName(
            Object obj, String fieldName, Object value) {
        try {
            Field field = getFieldByFieldName(obj, fieldName);
            if (field != null) {
                makeAccessible(field);
                field.set(obj, value);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            logger.error("setValueByFieldName error", e);
        }
        return false;
    }

    /**
     * 得到指定类型的指定位置的泛型实参
     * @param clazz 对象信息
     * @param <T>   泛型
     * @return 对象信息
     */
    public static <T> Class<T> findParameterizedType(Class<?> clazz) {
        Type parameterizedType = clazz.getGenericSuperclass();
        //CGLUB subclass target object(泛型在父类上)
        if (!(parameterizedType instanceof ParameterizedType)) {
            parameterizedType = clazz.getSuperclass().getGenericSuperclass();
        }
        if (!(parameterizedType instanceof ParameterizedType)) {
            return null;
        }
        Type[] actualTypeArguments = ((ParameterizedType) parameterizedType).getActualTypeArguments();
        if (actualTypeArguments == null || actualTypeArguments.length == 0) {
            return null;
        }
        return (Class<T>) actualTypeArguments[0];
    }

    /**
     * Make the given field accessible, explicitly setting it accessible if necessary
     * @param field 属性
     */
    @SuppressWarnings("deprecation")
    public static void makeAccessible(Field field) {
        if ((!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers()) || Modifier.isFinal(field.getModifiers())) && !field.isAccessible()) {
            field.setAccessible(true);
        }
    }
}
