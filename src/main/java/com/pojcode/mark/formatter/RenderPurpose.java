package com.pojcode.mark.formatter;

public enum RenderPurpose {
    FORMAT,
    TRANSLATION_SPANS,
    TRANSLATED_SPANS,
    TRANSLATED,
}
