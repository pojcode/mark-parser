package com.pojcode.mark.formatter;

public enum FormattingPhase {
    COLLECT,
    DOCUMENT_FIRST,
    DOCUMENT_TOP,
    DOCUMENT,
    DOCUMENT_BOTTOM,
}
