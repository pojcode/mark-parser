package com.pojcode.mark.html;

public interface Disposable {
    void dispose();
}
