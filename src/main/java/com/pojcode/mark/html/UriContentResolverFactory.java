package com.pojcode.mark.html;

import com.pojcode.mark.html.renderer.LinkResolverBasicContext;
import com.vladsch.flexmark.util.dependency.Dependent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.function.Function;

public interface UriContentResolverFactory extends Function<LinkResolverBasicContext, UriContentResolver>, Dependent {
    @Override
    @Nullable Set<Class<?>> getAfterDependents();

    @Override
    @Nullable Set<Class<?>> getBeforeDependents();

    @Override
    boolean affectsGlobalScope();

    @Override
    @NotNull UriContentResolver apply(@NotNull LinkResolverBasicContext context);
}
