package com.pojcode.mark.html;

import com.pojcode.mark.html.renderer.LinkResolverBasicContext;
import com.pojcode.mark.html.renderer.ResolvedContent;
import com.vladsch.flexmark.util.ast.Node;
import org.jetbrains.annotations.NotNull;

public interface UriContentResolver {
    @NotNull ResolvedContent resolveContent(@NotNull Node node, @NotNull LinkResolverBasicContext context, @NotNull ResolvedContent content);

    UriContentResolver NULL = (node, context, content) -> content;
}
