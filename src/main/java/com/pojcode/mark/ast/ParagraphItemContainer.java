package com.pojcode.mark.ast;

import com.pojcode.mark.parser.ListOptions;
import com.vladsch.flexmark.util.data.DataHolder;

public interface ParagraphItemContainer {
    boolean isParagraphInTightListItem(Paragraph node);
    boolean isItemParagraph(Paragraph node);
    boolean isParagraphWrappingDisabled(Paragraph node, ListOptions listOptions, DataHolder options);
}
