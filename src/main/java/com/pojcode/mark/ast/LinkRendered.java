package com.pojcode.mark.ast;

/**
 * Nodes which are textually derived from LinkRef
 */
public interface LinkRendered extends LinkRefDerived {

}
