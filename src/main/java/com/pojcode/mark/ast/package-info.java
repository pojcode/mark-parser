/**
 * AST node types (see {@link com.vladsch.flexmark.util.ast.Node}) and visitors (see {@link com.vladsch.flexmark.util.ast.NodeVisitor})
 */
package com.pojcode.mark.ast;
