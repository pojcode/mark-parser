package com.pojcode.mark.ast.util;

import com.pojcode.mark.ast.HtmlInnerBlock;
import com.pojcode.mark.ast.HtmlInnerBlockComment;
import com.vladsch.flexmark.util.ast.VisitHandler;

public class HtmlInnerVisitorExt {
    public static <V extends HtmlInnerVisitor> VisitHandler<?>[] VISIT_HANDLERS(V visitor) {
        return new VisitHandler<?>[] {
                new VisitHandler<>(HtmlInnerBlock.class, visitor::visit),
                new VisitHandler<>(HtmlInnerBlockComment.class, visitor::visit),
        };
    }
}
