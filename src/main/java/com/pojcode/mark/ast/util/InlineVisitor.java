package com.pojcode.mark.ast.util;

import com.pojcode.mark.ast.*;

public interface InlineVisitor {
    void visit(AutoLink node);
    void visit(Code node);
    void visit(Emphasis node);
    void visit(HardLineBreak node);
    void visit(HtmlEntity node);
    void visit(HtmlInline node);
    void visit(HtmlInlineComment node);
    void visit(Image node);
    void visit(ImageRef node);
    void visit(Link node);
    void visit(LinkRef node);
    void visit(MailLink node);
    void visit(SoftLineBreak node);
    void visit(StrongEmphasis node);
    void visit(Text node);
}
