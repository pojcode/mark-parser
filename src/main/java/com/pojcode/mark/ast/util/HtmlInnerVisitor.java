package com.pojcode.mark.ast.util;

import com.pojcode.mark.ast.HtmlInnerBlock;
import com.pojcode.mark.ast.HtmlInnerBlockComment;

public interface HtmlInnerVisitor {
    void visit(HtmlInnerBlock node);
    void visit(HtmlInnerBlockComment node);
}
