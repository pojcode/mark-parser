package com.pojcode.mark.ast.util;

import com.pojcode.mark.ast.ImageRef;
import com.pojcode.mark.ast.LinkRef;
import com.pojcode.mark.ast.RefNode;
import com.pojcode.mark.ast.Reference;
import com.pojcode.mark.parser.Parser;
import com.vladsch.flexmark.util.ast.KeepType;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.ast.NodeRepository;
import com.vladsch.flexmark.util.data.DataHolder;
import com.vladsch.flexmark.util.data.DataKey;
import com.vladsch.flexmark.util.sequence.Escaping;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

public class ReferenceRepository extends NodeRepository<Reference> {

    public ReferenceRepository(DataHolder options) {
        super(Parser.REFERENCES_KEEP.get(options));
    }

    @NotNull
    @Override
    public DataKey<ReferenceRepository> getDataKey() {
        return Parser.REFERENCES;
    }

    @NotNull
    @Override
    public DataKey<KeepType> getKeepDataKey() {
        return Parser.REFERENCES_KEEP;
    }

    @NotNull
    @Override
    public String normalizeKey(@NotNull CharSequence key) {
        return Escaping.normalizeReference(key, true);
    }

    @NotNull
    @Override
    public Set<Reference> getReferencedElements(Node parent) {
        HashSet<Reference> references = new HashSet<>();
        visitNodes(parent, value -> {
            if (value instanceof RefNode) {
                Reference reference = ((RefNode) value).getReferenceNode(ReferenceRepository.this);
                if (reference != null) {
                    references.add(reference);
                }
            }
        }, LinkRef.class, ImageRef.class);
        return references;
    }
}
