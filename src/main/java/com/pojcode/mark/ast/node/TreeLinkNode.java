package com.pojcode.mark.ast.node;

import com.pojcode.mark.ast.Link;
import com.pojcode.mark.ast.Text;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import com.pojcode.mark.utils.tree.Node;

/**
 * tree link node
*/
public class TreeLinkNode extends Link implements Node {

    private String id;
    private String parentId;
    private String text;
    private String title;
    private String url;

    public TreeLinkNode(Link link) {
        this.text = link.getText().toStringOrNull();
        this.title = link.getTitle().toStringOrNull();
        this.url = link.getUrl().toStringOrNull();
    }

    public TreeLinkNode(Text text) {
        this.text = text.getChars().toStringOrNull();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Node setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    @Override
    public Node setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    @Override
    public CharSequence getName() {
        return this.text;
    }

    @Override
    public Node setName(CharSequence name) {
        return this;
    }

    @Override
    public Comparable<?> getWeight() {
        return this.id;
    }

    @Override
    public Node setWeight(Comparable<?> weight) {
        return this;
    }

    @Override
    public BasedSequence getText() {
        return BasedSequence.of(this.text);
    }

    @Override
    public BasedSequence getTitle() {
        return BasedSequence.of(this.title);
    }

    @Override
    public BasedSequence getUrl() {
        return BasedSequence.of(this.url);
    }

    public String getStrText() {
        return this.text;
    }

    public String getStrTitle() {
        return this.title;
    }

    public String getStrUrl() {
        return this.url;
    }

    @Override
    public String[] extraFields() {
        return new String[] {
            "text", "title", "url"
        };
    }
}
