package com.pojcode.mark.ast;

public interface ParagraphContainer {
    boolean isParagraphEndWrappingDisabled(Paragraph node);
    boolean isParagraphStartWrappingDisabled(Paragraph node);
}
