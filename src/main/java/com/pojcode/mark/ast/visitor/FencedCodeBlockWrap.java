package com.pojcode.mark.ast.visitor;

import com.vladsch.flexmark.util.sequence.BasedSequence;

public class FencedCodeBlockWrap {

    private BasedSequence openingMarker = BasedSequence.NULL;
    private BasedSequence prefixOpeningMarker = BasedSequence.NULL;
    private BasedSequence suffixOpeningMarker = BasedSequence.NULL;
    private BasedSequence closingMarker = BasedSequence.NULL;
    private BasedSequence prefixClosingMarker = BasedSequence.NULL;
    private BasedSequence suffixClosingMarker = BasedSequence.NULL;
    private BasedSequence info = BasedSequence.NULL;
    private BasedSequence content = BasedSequence.NULL;

    public static FencedCodeBlockWrap of() {
        return new FencedCodeBlockWrap();
    }

    public FencedCodeBlockWrap openingMarker(BasedSequence openingMarker) {
        if (openingMarker != null) {
            this.openingMarker = openingMarker;
        }
        return this;
    }

    public FencedCodeBlockWrap prefixOpeningMarker(BasedSequence prefixOpeningMarker) {
        if (prefixOpeningMarker != null) {
            this.prefixOpeningMarker = prefixOpeningMarker;
        }
        return this;
    }

    public FencedCodeBlockWrap suffixOpeningMarker(BasedSequence suffixOpeningMarker) {
        if (suffixOpeningMarker != null) {
            this.suffixOpeningMarker = suffixOpeningMarker;
        }
        return this;
    }

    public FencedCodeBlockWrap closingMarker(BasedSequence closingMarker) {
        if (closingMarker != null) {
            this.closingMarker = closingMarker;
        }
        return this;
    }

    public FencedCodeBlockWrap prefixClosingMarker(BasedSequence prefixClosingMarker) {
        if (prefixClosingMarker != null) {
            this.prefixClosingMarker = prefixClosingMarker;
        }
        return this;
    }

    public FencedCodeBlockWrap suffixClosingMarker(BasedSequence suffixClosingMarker) {
        if (suffixClosingMarker != null) {
            this.suffixClosingMarker = suffixClosingMarker;
        }
        return this;
    }

    public FencedCodeBlockWrap content(BasedSequence content) {
        this.content = content;
        return this;
    }

    public FencedCodeBlockWrap info(BasedSequence info) {
        if (info != null) {
            this.info = info;
        }
        return this;
    }

    public BasedSequence getOpeningMarker() {
        return openingMarker;
    }

    public BasedSequence getPrefixOpeningMarker() {
        return prefixOpeningMarker;
    }

    public BasedSequence getSuffixOpeningMarker() {
        return suffixOpeningMarker;
    }

    public BasedSequence getClosingMarker() {
        return closingMarker;
    }

    public BasedSequence getPrefixClosingMarker() {
        return prefixClosingMarker;
    }

    public BasedSequence getSuffixClosingMarker() {
        return suffixClosingMarker;
    }

    public BasedSequence getInfo() {
        return info;
    }

    public BasedSequence getContent() {
        return content;
    }
}
