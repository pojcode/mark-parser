package com.pojcode.mark.ast.visitor;

import com.pojcode.mark.ast.FencedCodeBlock;
import com.vladsch.flexmark.util.ast.VisitHandler;
import com.vladsch.flexmark.util.sequence.BasedSequence;

import java.util.function.Consumer;

public class BlockWrapVisit {

    public static VisitHandler<FencedCodeBlock> fencedCodeBlockWrap(Consumer<FencedCodeBlockWrap> consumer) {
        return new VisitHandler<>(FencedCodeBlock.class, node -> {
            FencedCodeBlockWrap wrap = FencedCodeBlockWrap.of()
                    .openingMarker(node.getOpeningMarker())
                    .closingMarker(node.getClosingMarker())
                    .content(node.getContentChars())
                    .info(node.getInfo());
            consumer.accept(wrap);
            BasedSequence openingMarker = wrap.getPrefixOpeningMarker().append(wrap.getOpeningMarker(), wrap.getSuffixOpeningMarker());
            BasedSequence closingMarker = wrap.getPrefixClosingMarker().append(wrap.getClosingMarker(), wrap.getSuffixClosingMarker());
            node.setOpeningMarker(openingMarker);
            node.setClosingMarker(closingMarker);
            node.setInfo(wrap.getInfo());
            node.setChars(openingMarker.append(wrap.getInfo(), BasedSequence.EOL, wrap.getContent(), closingMarker));
        });
    }

}
