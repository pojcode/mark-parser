package com.pojcode.mark.ext.toc;

public interface TocVisitor {
    void visit(TocBlock node);
}
