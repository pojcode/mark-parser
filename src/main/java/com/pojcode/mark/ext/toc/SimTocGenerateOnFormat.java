package com.pojcode.mark.ext.toc;

public enum SimTocGenerateOnFormat {
    AS_IS,
    UPDATE,
    REMOVE,
}
