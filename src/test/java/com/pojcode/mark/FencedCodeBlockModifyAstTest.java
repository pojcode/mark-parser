package com.pojcode.mark;

import com.pojcode.mark.ast.visitor.BlockWrapVisit;
import com.pojcode.mark.formatter.Formatter;
import com.pojcode.mark.parser.Parser;
import com.vladsch.flexmark.util.ast.Document;
import com.vladsch.flexmark.util.ast.NodeVisitor;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.vladsch.flexmark.util.sequence.BasedSequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FencedCode Block Modify Ast Test
*/
public class FencedCodeBlockModifyAstTest {

    private static final Logger logger = LoggerFactory.getLogger(FencedCodeBlockModifyAstTest.class);

    private static String INPUT = "```bash\n" +
            "$ mkdir test\n" +
            "$ touch a.txt\n" +
            "```\n" +
            "\n" +
            "```bash\n" +
            "$ pwd\n" +
            "```";

    public static void main(String[] args) {
        MutableDataSet options = new MutableDataSet();
        Parser parser = Parser.builder(options).build();
        /** Option Formatter To Markdown Keep Original Marker */
        options.set(Formatter.FENCED_CODE_KEEP_ORIGINAL_MARKER, true);
        Document document = parser.parse(INPUT);

        /** Node Visitor Modify FencedCodeBlock Marker */
        NodeVisitor nodeVisitor = new NodeVisitor(BlockWrapVisit.fencedCodeBlockWrap(wrap ->
                wrap.suffixClosingMarker(BasedSequence.of("{{copy exec}}"))
        ));
        nodeVisitor.visit(document);

        Formatter formatter = Formatter.builder(options).build();
        String newMarkdown = formatter.render(document);
        logger.info("\n" + newMarkdown);
    }

}
