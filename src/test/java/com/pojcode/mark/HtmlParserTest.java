package com.pojcode.mark;

import com.pojcode.mark.html.HtmlRenderer;
import com.pojcode.mark.parser.Parser;
import com.vladsch.flexmark.util.data.MutableDataSet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Html Parser
 */
public class HtmlParserTest {

    private static final Logger logger = LoggerFactory.getLogger(HtmlParserTest.class);

    public static void main(String[] args) {
        String html = "<p><strong>&lt;a target=\"_blank\"&gt;我是链接&lt;/a&gt;</strong></p>";
        // 方式一：使用正则表达式替换标签（存在有转义字符，使用其他工具进行转换）
        MutableDataSet options = new MutableDataSet();
        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();
        String htmlContent = renderer.render(parser.parse(html));
        logger.info(htmlContent.replaceAll("<[^>]+>", ""));

        // 方式二：使用jsoup HTML Parser
        Document document = Jsoup.parse(html);
        logger.info(document.text());
    }

}
