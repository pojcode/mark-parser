package com.pojcode.mark;

import com.pojcode.mark.ext.tables.TablesExtension;
import com.pojcode.mark.ext.toc.TocExtension;
import com.pojcode.mark.parser.Parser;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.pojcode.mark.utils.tree.TreeNode;
import com.pojcode.mark.utils.tree.TreeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Bull List Parser Convert Test
 */
public class BullListParserTest {

    private static final Logger logger = LoggerFactory.getLogger(BullListParserTest.class);

    public static void main(String[] args) {
        String markdown = "- [寻找牛刀，以便小试](first-try/intro.md)\n" +
                "  - [安装 Rust 环境](first-try/installation.md)\n" +
                "  - [墙推 VSCode!](first-try/editor.md)\n" +
                "  - [认识 Cargo](first-try/cargo.md)\n" +
                "  - [不仅仅是 Hello world](first-try/hello-world.md)\n" +
                "  - [下载依赖太慢了？](first-try/slowly-downloading.md)";
        MutableDataSet options = new MutableDataSet();
        options.set(Parser.EXTENSIONS, Arrays.asList(
                TablesExtension.create(),
                TocExtension.create()
        ));
        // 树解析启用加回调
        options.set(Parser.LISTS_ITEM_BUILD_TREE_ENABLE, true);
        options.set(Parser.LISTS_ITEM_BUILD_TREE_PROCESSOR_CALLBACK, (options1, document, nodeList) -> {
            List<TreeNode> build = TreeUtil.build(nodeList);
            build.forEach(treeNode -> logger.info("\n" + treeNode.toString()));
        });
        Parser parser = Parser.builder(options).build();
        parser.parse(markdown);
    }

}
